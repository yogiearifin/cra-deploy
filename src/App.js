import React, { useState, useEffect } from "react";
import "./App.css";
import axios from "axios";

function App() {
  const token = localStorage.getItem("token");
  console.log("token", token);
  const [users, setUsers] = useState({});

  const [register, setRegister] = useState({
    nama: "",
    email: "",
    password: "",
    confirmPassword: "",
  });

  const [isLogin, setIsLogin] = useState(false);
  console.log("is login", isLogin);

  const baseUrl = "https://team-c.gabatch11.my.id/";

  const [login, setLogin] = useState({
    email: "",
    password: "",
  });

  console.log(login);

  const inputLogin = (e) => {
    setLogin({
      ...login,
      [e.target.name]: e.target.value,
    });
  };

  const submitLogin = () => {
    axios
      .post(`${baseUrl}user/signin/`, login, {
        headers: {
          "Content-Type": "application/json",
        },
      })
      .then(
        (res) =>
          // console.log(res)
          localStorage.setItem("token", res.data.token),
        setIsLogin(true)
      );
  };

  const changeInput = (e) => {
    setRegister({ ...register, [e.target.name]: e.target.value });
  };

  // console.log(register);

  const submitRegister = () => {
    axios
      .post(`${baseUrl}user/signup/`, register, {
        headers: {
          "Content-Type": "application/json",
        },
      })
      .then((res) => console.log(res));
  };

  const getUser = () => {
    axios
      .get(
        `${baseUrl}user/
    `,
        {
          headers: {
            authorization: `Bearer ${token}`,
          },
        }
      )
      .then((res) => setUsers(res.data.data));
  };

  console.log("users", users);

  const logoutEvent = () => {
    localStorage.clear();
    setIsLogin(false);

    // redirect
  };

  useEffect(() => {
    getUser();
  }, []);

  useEffect(() => {
    if (token) {
      setIsLogin(true);
    }
  }, [token]);

  console.log(isLogin);

  return (
    <>
      <div className="App">
        {isLogin ? (
          <div>
            <h1>Welcome {users.nama}  </h1>
          </div>
        ) : (
          <>
            <div>
              <h1>Register</h1>
              <div className="app-div">
                <p>Name</p>
                <input name="nama" onChange={changeInput} />
              </div>
              <div className="app-div">
                <p>Email</p>
                <input name="email" onChange={changeInput} />
              </div>
              <div className="app-div">
                <p>Password</p>
                <input type="password" name="password" onChange={changeInput} />
              </div>
              <div className="app-div">
                <p>Confirm Password</p>
                <input
                  type="password"
                  name="confirmPassword"
                  onChange={changeInput}
                />
              </div>
              <div className="app-div">
                <button onClick={submitRegister}>Sign Up</button>
              </div>
            </div>
            <div>
              <h1>Login</h1>
              <div className="app-div">
                <p>Email</p>
                <input name="email" onChange={inputLogin} />
              </div>
              <div className="app-div">
                <p>Password</p>
                <input type="password" name="password" onChange={inputLogin} />
              </div>
              <div className="app-div">
                <button onClick={submitLogin}>Login</button>
              </div>
            </div>
          </>
        )}
        <div>
          <h1>Review</h1>
          <button onClick={logoutEvent}>Logout</button>
        </div>
      </div>
    </>
  );
}

export default App;
